﻿# Updated: 12/11/2020
# Author: Sunny Kahlon
# This script parses list of display names (displaynames.txt) and outputs sameaccountnames.csv
# listing all enable and disabled samaccountnames for each displayname

$aResults = @()
$List = Get-Content “$PSScriptRoot\displaynames.txt”
ForEach($Item in $List){
$Item = $Item.Trim()
$enabledUsers = Get-ADUser -Filter{displayName -like $Item -and Enabled -eq $True} -Properties SamAccountName
$disabledUsers = Get-ADUser -Filter{displayName -like $Item -and Enabled -eq $False} -Properties SamAccountName
$hItemDetails = New-Object -TypeName psobject -Property @{
DisplayName = $Item
EnabledAccounts = $enabledUsers.SamAccountName
DisabledAccounts = $disabledUsers.SamAccountName
}
#Add data to array
$aResults += $hItemDetails
}
$aResults | Export-CSV “$PSScriptRoot\samaccountnames.csv” -NoTypeInformation