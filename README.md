### Function:
Pulls samaccountnames using displaynames and outputs [*EnabledAccounts, DisabledAccounts* and *DisplayName* columns] in samaccountnames.csv file

### How to use:
list display names of users in `displaynames.txt` as such as:
>John Smith<br>
Jane Doe

Save `displaynames.txt` and now open PowerShell, load *AD module*and run `get-samaccountnames.ps1`
<br>it will read all names in `displaynames.txt` convert them to samaccountnames and outputs samaccountnames.csv in the working directory.
<br>


